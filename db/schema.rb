# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140221181436) do

  create_table "clubs", force: true do |t|
    t.string "name"
    t.string "link"
    t.string "email"
    t.string "bank_account"
  end

  create_table "performances", force: true do |t|
    t.string   "place"
    t.integer  "rows"
    t.integer  "seats"
    t.integer  "total_seats"
    t.text     "disabled"
    t.datetime "time"
    t.integer  "play_id"
    t.integer  "price"
    t.integer  "play_of_day"
  end

  create_table "plays", force: true do |t|
    t.string  "name"
    t.string  "link"
    t.integer "club_id"
  end

  create_table "reservations", force: true do |t|
    t.integer  "performance_id"
    t.string   "name"
    t.string   "email"
    t.integer  "count"
    t.boolean  "played"
    t.datetime "time"
    t.integer  "vs"
    t.string   "surname"
  end

  create_table "seats", force: true do |t|
    t.integer "reservation_id"
    t.integer "row"
    t.integer "seat"
  end

  create_table "suggestions", force: true do |t|
    t.string   "email"
    t.string   "title"
    t.text     "text"
    t.integer  "state"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name",                default: "", null: false
    t.string   "email",               default: "", null: false
    t.string   "encrypted_password",  default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["name"], name: "index_users_on_name", unique: true

end
