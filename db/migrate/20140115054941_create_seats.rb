class CreateSeats < ActiveRecord::Migration
  def change
    create_table :seats do |t|
    	t.belongs_to   :reservation
    	t.integer      :row
    	t.integer      :seat
    end
  end
end
