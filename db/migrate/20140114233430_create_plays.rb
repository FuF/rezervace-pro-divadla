class CreatePlays < ActiveRecord::Migration
  def change
    create_table :plays do |t|
    	t.string        :name
    	t.string        :link
    	t.belongs_to    :club
    end
  end
end
