class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
    	t.belongs_to   :performance
      t.string       :name
      t.string       :email
      t.integer      :count
    end
  end
end
