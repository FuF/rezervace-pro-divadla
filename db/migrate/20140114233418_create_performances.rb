class CreatePerformances < ActiveRecord::Migration
  def change
    create_table :performances do |t|
    	t.string        :place
    	t.integer       :rows
    	t.integer       :seats
    	t.integer       :total_seats
    	t.text          :disabled
    	t.datetime      :time
    	t.belongs_to    :play 
    end
  end
end
