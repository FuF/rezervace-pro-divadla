class NewColReservation < ActiveRecord::Migration
  def change
  	add_column :Reservations, :played, :boolean
  	add_column :Reservations, :time, :datetime
  	add_column :Reservations, :vs, :integer
  end
end
