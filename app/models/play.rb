class Play < ActiveRecord::Base
	belongs_to :club
	has_many   :performances
end
