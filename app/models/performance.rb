class Performance < ActiveRecord::Base
	belongs_to :play
	has_many :reservations
	def datum
		time.strftime( "%-d.%-m.%Y %H:%M")
	end
end
