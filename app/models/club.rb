class Club < ActiveRecord::Base
	has_many :plays
	has_many :performances, through: :plays

end
