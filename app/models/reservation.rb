class Reservation < ActiveRecord::Base
	belongs_to :performance
	has_many :seats
	before_save :generate_vs

	def generate_vs
		play_of_day = Performance.where( :time => self.performance.time.day..self.performance.time.day ).all.count + 1
		user_count  = Reservation.where( performance_id: self.performance_id ).all.count
		self.vs = self.performance.time.strftime( "%y%m%d") + play_of_day.to_s + user_count.to_s.rjust( 3, "0")
	end

end
