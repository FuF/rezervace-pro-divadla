r = @reservations.map do |rez|
	[
		rez.name,
		rez.email,
		rez.vs,
		rez.seats.map {|s| (s.row + 'A'.ord).chr + s.seat.to_s }.join(' ')
	]
end 

prawn_document do |pdf|
	pdf.table r, { :width => pdf.bounds.width, :cell_style => { :size => 10, :padding => 2 } }
	pdf.number_pages @play + "                Strana <page>/<total>", { :align => :right,
																							:at => [pdf.bounds.left, 0 ],
																							:size => 10
																						 }
end