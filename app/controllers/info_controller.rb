class InfoController < ApplicationController
	def index
		if params[:id] then
			@reservations = Reservation.where( :performance_id => params[:id]).order("id DESC").all
			p = Performance.find(params[:id])
			@play = p.play.name + " - " + p.place + " - " + p.datum

			respond_to do |format|
				format.html {render "list"}
				format.pdf {render "list" }
			end
		else
			@performances = Performance.all
		end


		
	end
end