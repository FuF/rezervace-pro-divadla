class ListController < ApplicationController
	def index
		club_link = params[:club]

		if club_link.nil? then
			@performances = Performance.where( "time > ?", Time.now ).order("id DESC").all
		else
			club = Club.where( link: club_link ).first
			@club_name = club.name
			@performances = club.performances
		end
	end
end
