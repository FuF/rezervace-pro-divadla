class RezervaceController < ApplicationController

	def index
		@errors = Array.new
		@performance = Performance.find_by_id(params[:id])
		if @performance.nil? then
			@errors.push({:title => "Představení s tímto ID neexistuje!", :text => "Zkontrolujte odkaz"})
		else
			@taken = Array.new
			@performance.reservations.each do |r|
				r.seats.each do |s|
					if @taken[s.row].nil? then @taken[s.row] = Array.new end
					@taken[s.row].push(s.seat)
				end
			end
		end
	end


	def check
		@errors = Array.new

		@seats = params[:seat]

		@totalseats = 0

		if params[:seat].nil? then
			@errors.push({:title => "Nebyla vybrána žádna sedadla!", :text => ""})
		else
			@seats.each do |rada,sedadla|
				sedadla.each do |sedadlo|
					@totalseats = @totalseats + 1
				end
			end
		end
		if params[:email] == "" then
			@errors.push({:title => "Nebyl zadán email!", :text => ""})
		end
		if params[:jmeno] == "" then
			@errors.push({:title => "Nebylo zadáno Vaše jméno!", :text => ""})
		end

		


		session[:seats] = @seats
		session[:name] = params[:jmeno]
		session[:email] = params[:email]
		session[:id] = params[:id]

		@performance = Performance.find(params[:id])
	rescue ActiveRecord::RecordNotFound => e
		render nothing: true
		@errors << ["Představení s tímto ID neexistuje!", "Zkontrolujte odkaz"]
		
	end

	def done
		seats = Array.new
		r = Reservation.new
		r.performance_id = session[:id]
		r.name = session[:name]
		r.email = session[:email]
		r.time = Time.now

		i = 0

		ok = 1
		session[:seats].each do |rada,sedadla|
			sedadla.each do |sedadlo|
				@x = Seat.where(row: ( rada.ord - 'a'.ord ), seat: sedadlo[0].to_i ).to_a
				@x.each do |xx|
					if xx.reservation.performance.id == r.performance.id
						ok = 0
						break
					end
				end
				s = Seat.new
				s.row = ( rada.ord - 'a'.ord )
				s.seat = sedadlo[0].to_i
				seats << s
				i = i + 1
			end
		end

		if ok == 1 then
			r.count = i
			r.save

			seats.each do |seat|
				seat.reservation_id = r.id
				seat.save
			end

			@seats = seats
			@r = r

			Email.new_reservation( r ).deliver

			redirect_to "/ok"
		end
	end
end
