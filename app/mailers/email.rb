class Email < ActionMailer::Base
  default from: "Rezervace lístků <info@rezervace-listku.cz>"

  def new_reservation( reservation )
  	@reservation = reservation
  	mail( to: reservation.email, sender: "Rezervace lístků <info@rezervace-listku.cz>", subject: "Rezervace na hru \"" + reservation.performance.play.name + "\"")
  end
end
