$(document).ready( function($) {
	function get_checked() {
		var values = $('input:checked').map( function () {
			var rada = this.value.split('|')[0];
			var sedadlo = this.value.split('|')[1];
			return ( "řada: " + rada + " sedadlo: " + sedadlo ) ;
		}).get();
		$('#listky').html( values.join( "<br>") );
	}

	function count_checked() {
		var count = $( 'input:checked').length;
		var price = $( '#vstupne').text();
		$( '#pocet_listku').text( count );

		if( price == "Vstupné dobrovolné" ) {
			$( '#celkem').text( price );
		}
		else {
			$( '#celkem').text( price * count );
		}
	}

	$('.seat').click( function(){
		$(this).toggleClass('btn-success');
		$(this).toggleClass('btn-primary');
		$(this).toggleClass('active');
		$(this).find('input[type="checkbox"]').prop( 'checked', !$(this).find('input[type="checkbox"]').is(':checked') );
//		get_checked();
		count_checked();
		return false;
	});

});
